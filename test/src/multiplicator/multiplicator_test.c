#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include "multiplicator.h"

static void test_fibonacci(void **state) {
    assert_int_equal(8, multiply(4, 2));
}

int main(int argc, char **argv) {
    const struct CMUnitTest tests[] =
    {
        cmocka_unit_test(test_fibonacci),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
