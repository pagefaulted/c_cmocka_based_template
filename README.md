# C template project

This is a template project for C.

The build tool is _CMake_.
The unit test library is [Cmocka](https://cmocka.org/).

## Depencencies

The only dependency is Cmocka, which should be available in your distro's package manager.

For example, with Arch:

```
pacman -S cmocka
```

## Example commands

These commands will build the project as well as the tests and execute them:

```
mkdir build
cd build
cmake ../
cmake --build .
ctest .
```
This is the output:

```
Test project /home/mods/development/c_cmocka_based_template/build
    Start 1: fibonacci
1/2 Test #1: fibonacci ........................   Passed    0.00 sec
    Start 2: multiplicator
2/2 Test #2: multiplicator ....................   Passed    0.00 sec

100% tests passed, 0 tests failed out of 2

Total Test time (real) =   0.01 sec
```
